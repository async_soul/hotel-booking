import { MaterialModule } from './modules/material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HomeComponent } from './components/home/home.component';
import { BookingDetailsComponent } from './components/booking-details/booking-details.component';
import { HotelListComponent } from './components/hotel-list/hotel-list.component';
import { BookingComponent } from './components/booking/booking.component';
import { SuccessComponent } from './components/success/success.component';

const routes: Routes = [
  {
    path: '', component: HomeComponent, pathMatch: 'full'
  },
  {
    path: 'home', component: HomeComponent
  },
  {
    path: 'booking-details', component: BookingDetailsComponent
  },
  {
    path: 'hotels', component: HotelListComponent
  },
  {
    path: 'booking', component: BookingComponent
  },
  {
    path: 'success', component: SuccessComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BookingDetailsComponent,
    HotelListComponent,
    BookingComponent,
    SuccessComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
