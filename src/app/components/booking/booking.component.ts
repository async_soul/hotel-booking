import { CommonService } from './../../services/common.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit {


  guestData = {
    guestList: [],
    email: '',
    mobileNumber: ''
  };
  countrycode = '';

  constructor(
    private router: Router,
    private commonService: CommonService
  ) { }

  ngOnInit(): void {
    this.guestData.guestList.push({ title: '', firstName: '', lastName: '' });
  }

  validateBooking() {
    console.log(this.guestData);
    if (this.guestData.guestList[0].title.length <= 0) {
      this.commonService.openSnackBar('Please choose title of guest');
      return;
    } else if (this.guestData.guestList[0].firstName.length <= 0) {
      this.commonService.openSnackBar('Please enter first name of guest');
      return;
    } else if (this.guestData.guestList[0].lastName.length <= 0) {
      this.commonService.openSnackBar('Please enter last name of guest');
      return;
    } else if (!this.guestData.email.match(/[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$/)) {
      this.commonService.openSnackBar('Please enter a valid email address');
      return;
    } else if (!this.guestData.mobileNumber.match(/[0-9]{10}$/)) {
      this.commonService.openSnackBar('Please enter a valid mobile number');
      return;
    } else {
      this.completeBooking();
    }
  }

  completeBooking(): void {
    console.log(JSON.stringify(this.guestData));
    this.guestData.mobileNumber = this.countrycode + this.guestData.mobileNumber;
    // Forward to Payment
    // On success
    // POST API - Book Hotel
    this.router.navigate(['success']);
  }

  addGuest(): void {
    const nameObject = { title: '', firstName: '', lastName: '' };
    this.guestData.guestList.push(nameObject);
  }

  removeGuest(idx) {
    console.log(idx);
    this.guestData.guestList.splice(idx, 1);
  }

}
