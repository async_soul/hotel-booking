import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  where: string = '';
  guestNumber: number;
  checkIn: any;
  checkOut: any;
  bookingReferenceNo: string;
  constructor(
    private router: Router,
    private commonService: CommonService
  ) { }

  ngOnInit(): void {
  }


  bookNow(): void {
    if(this.where && this.guestNumber && this.checkIn && this.checkOut){
      this.router.navigate(['hotels']);
    } else {
      this.commonService.openSnackBar('Reuired Input Missing')
    } 
  }

  viewBooking(): void {
    if(this.bookingReferenceNo){
      this.router.navigate(['booking-details']);
    } else {
      this.commonService.openSnackBar('Reuired Input Missing')
    } 
  }
}
