import { GenericHttpService } from './../../services/generic-http.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-booking-details',
  templateUrl: './booking-details.component.html',
  styleUrls: ['./booking-details.component.css']
})
export class BookingDetailsComponent implements OnInit {

  bookingDetails: any;
  panelOpenState = true;

  constructor(
    private httpService: GenericHttpService
  ) { }

  ngOnInit(): void {
    this.getBookingDetails();
  }

  getBookingDetails(): void {
    this.httpService.Get('v1/getBookingDetails/1').subscribe((success) => {
      if (success) {
        this.bookingDetails = success;
        console.log(this.bookingDetails);
      }
    }, error => {
      console.log(error);
    });
  }

}
