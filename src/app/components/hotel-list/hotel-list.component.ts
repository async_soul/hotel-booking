import { GenericHttpService } from './../../services/generic-http.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hotel-list',
  templateUrl: './hotel-list.component.html',
  styleUrls: ['./hotel-list.component.css']
})
export class HotelListComponent implements OnInit {

  hotelList: any;
  displayHotelList: any;

  // hotelList = [
  //   {
  //     id: 1,
  //     hotelName: 'Sandpiper Hotel',
  //     location: 'City Center',
  //     rating: 4.2,
  //     totalReviews: 130,
  //     amenities: ['Airport Transfer', 'Swimming Pool', 'Free Wifi', 'Parking'],
  //     imageUrl: 'https://firebasestorage.googleapis.com/v0/b/guinevere-53609.appspot.com/o/hotel1.jpeg?alt=media&token=61ec4c5c-2c7f-4db1-b6e7-c5b7fbbc062a'
  //   },
  //   {
  //     id: 2,
  //     hotelName: 'Cosmos Hotel',
  //     location: 'Near Airport',
  //     rating: 4.2,
  //     totalReviews: 130,
  //     amenities: ['Airport Transfer', 'Free Wifi', 'CCTV'],
  //     imageUrl: 'https://firebasestorage.googleapis.com/v0/b/guinevere-53609.appspot.com/o/hotel2.jpeg?alt=media&token=8f58a0f4-85c6-4ca7-acfe-11f0eb5b1741'
  //   },
  //   {
  //     id: 3,
  //     hotelName: 'Crossroads Hotel',
  //     location: 'Near Airport',
  //     rating: 4.2,
  //     totalReviews: 130,
  //     amenities: ['Banquet Hall', 'Swimming Pool', 'Free Wifi', 'Free Breakfast'],
  //     imageUrl: 'https://firebasestorage.googleapis.com/v0/b/guinevere-53609.appspot.com/o/hotel3.jpeg?alt=media&token=c8bd7461-2aff-4e29-b372-66a73246ebf0'
  //   },
  //   {
  //     id: 4,
  //     hotelName: 'KIP Hotel',
  //     location: 'City Center',
  //     rating: 4.2,
  //     totalReviews: 130,
  //     amenities: ['Airport Transfer', 'Banquet Hall', 'Swimming Pool', 'Superior Room'],
  //     imageUrl: 'https://firebasestorage.googleapis.com/v0/b/guinevere-53609.appspot.com/o/hotel4.jpeg?alt=media&token=d9e736e0-6b5a-40c0-b053-038e9ecc3ae5'
  //   },
  //   {
  //     id: 5,
  //     hotelName: 'Metropol Hotel',
  //     location: 'City Center',
  //     rating: 4.2,
  //     totalReviews: 130,
  //     amenities: ['Airport Transfer', 'Banquet Hall', 'Swimming Pool', 'Free Wifi', 'Superior Room'],
  //     imageUrl: 'https://firebasestorage.googleapis.com/v0/b/guinevere-53609.appspot.com/o/hotel4.jpeg?alt=media&token=d9e736e0-6b5a-40c0-b053-038e9ecc3ae5'
  //   }
  // ];


  checkFilter: any;
  filterBadge = '';

  constructor(
    private router: Router,
    private httpService: GenericHttpService
  ) { }

  ngOnInit(): void {
    this.initCheckboxFilter();
    this.getHotelList();
  }

  initCheckboxFilter(): void {
    this.checkFilter = {
      price: {
        below100: false,
        btw100500: false,
        above500: false
      },
      rating: {
        above3: false,
        above4: false
      }
    };
  }

  getHotelList(): void {
    this.httpService.Get('v1/getHotelList').subscribe((success) => {
      if (success) {
        this.hotelList = success;
        this.displayHotelList = success;
      }
    }, error => {
      console.log(error);
    });
  }

  bookHotel(): void {
    this.router.navigate(['booking']);
  }

  resetCheckbox(section: string): void {
    switch (section) {
      case 'ALL':
        this.initCheckboxFilter();
        this.filterList();
        break;
      case 'PRICE':
        this.checkFilter.price = {
          below100: false,
          btw100500: false,
          above500: false
        };
        this.filterList();
        break;
      case 'RATING':
        this.checkFilter.rating = {
          above3: false,
          above4: false
        };
        this.filterList();
        break;
      default:
        console.log('Exception');
    }
  }


  filterList(): void {
    const filterList = [];
    let finalList = [];
    for (const [key, value] of Object.entries(this.checkFilter.price)) {
      if (value) {
        filterList.push(key);
      }
    }
    if (!filterList.length) {
      if (this.checkFilter.rating.above3) {
        this.displayHotelList = this.hotelList.filter((o) => o.rating > 3);
      } else if (this.checkFilter.rating.above4) {
        this.displayHotelList = this.hotelList.filter((o) => o.rating > 4);
      } else {
        this.displayHotelList = this.hotelList;
      }
      return;
    }
    filterList.forEach((item) => {
      switch (item) {
        case 'below100':
          finalList = [...finalList, ...this.hotelList.filter((hotel) => hotel.price < 100)];
          break;
        case 'btw100500':
          finalList = [...finalList, ...this.hotelList.filter((hotel) => hotel.price >= 100 && hotel.price <= 500)];
          break;
        case 'above500':
          finalList = [...finalList, ...this.hotelList.filter((hotel) => hotel.price > 500)];
          break;
      }
    });
    if (finalList.length && this.checkFilter.rating.above3) {
      this.displayHotelList = finalList.filter((o) => o.rating > 3);
    } else if (finalList.length && this.checkFilter.rating.above4) {
      this.displayHotelList = finalList.filter((o) => o.rating > 4);
    } else {
      this.displayHotelList = finalList;
    }
  }

  sortList(sortType) {
    if (sortType === 'LOW') {
      this.displayHotelList = this.displayHotelList.sort((a, b) => {
        return a.price - b.price;
      });
      this.filterBadge = 'Price low to high';
    } else if (sortType === 'HIGH') {
      this.displayHotelList = this.displayHotelList.sort((a, b) => {
        return b.price - a.price;
      });
      this.filterBadge = 'Price high to low';
    } else {
      this.displayHotelList = this.displayHotelList.sort((a, b) => {
        return b.rating - a.rating;
      });
      this.filterBadge = 'Rating';
    }
  }

}
