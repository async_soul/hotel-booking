import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class GenericHttpService {

    baseEndPoint = 'http://demo4095604.mockable.io/';

    constructor(private httpclient: HttpClient) { }

    // GET
    Get(apiEndPoint: string, id?: any, queryString?: string): Observable<any> {
        let endPoint = this.baseEndPoint + apiEndPoint;
        if (id) {
            endPoint += '' + id;
        }
        if (queryString) {
            endPoint += '?' + queryString;
        }
        return this.httpclient.get(endPoint);
    }

    // POST
    Post(data: any, apiEndPoint: string): Observable<any> {
        return this.httpclient.post(this.baseEndPoint + apiEndPoint, data);
    }

    // POST
    PostEncoded(formdata: any, apiEndPoint: string, options: any): Observable<any> {
        return this.httpclient.post(this.baseEndPoint + apiEndPoint, formdata, options);
    }

    // PUT
    Put(data: any, apiEndPoint: string): Observable<any> {
        return this.httpclient.put(this.baseEndPoint + apiEndPoint, data);
    }

    // DELETE
    Delete(apiEndPoint: string): Observable<any> {
        return this.httpclient.delete(this.baseEndPoint + apiEndPoint);
    }


    GetMap(apiEndPoint: string, id?: any, queryString?: string): Observable<any> {
        let endPoint = apiEndPoint;
        if (id) {
            endPoint += '' + id;
        }
        if (queryString) {
            endPoint += '?' + queryString;
        }
        return this.httpclient.get(endPoint);
    }
}
