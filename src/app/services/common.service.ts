import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
    providedIn: 'root'
})
export class CommonService {

    constructor(public snackBar: MatSnackBar) { }

    openSnackBar(message: string) {
        this.snackBar.open(message, 'OK', {
            duration: 3000,
        });
    }

}
